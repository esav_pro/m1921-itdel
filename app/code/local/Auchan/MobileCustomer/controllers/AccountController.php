<?php

/**
 * @author  Andrii Yesikov <callmeandrey@gmail.com>
 * Class Auchan_MobileCustomer_AccountController
 */
require_once(Mage::getModuleDir('controllers','Mage_Customer').DS.'AccountController.php');
class Auchan_MobileCustomer_AccountController extends Mage_Customer_AccountController{

    //
    public function IndexAction() {
        die('die IndexAction() Auchan_MobileCustomer_AccountController ');
    }

    /**
     * Login customer action
     * POST DOMEN/mobile-customer/account/login/?key=GeJtDzII017yM&username=test@test.com&password=123456&frontend=81fbkts0mp1m4k4q3gsdlep3h0
     */
    public function LoginAction() {
        if (!$this->getRequest()->isPost()) { // IF NOT POST
            $response['error'] = $this->__('Request is not POST.');
        }elseif($this->_getSession()->isLoggedIn()){ // IF LOGGED IN
            /** @var Mage_Customer_Model_Session $session */
            $session = $this->_getSession();
            $response['success'] = Mage::helper('mobilecustomer')->__('The user has been already logged in.');
            $response['error'] = Mage::helper('mobilecustomer')->__('The user has been already logged in.');
            $response['CookieLifetime']=$session->getCookieLifetime();
            $response['frontend']=$session->getSessionIdForHost();
        } else{
            $_key = Mage::helper("mobilecustomer")->getKey();
            $key  = $this->getRequest()->getParam('key');
            $username = $this->getRequest()->getParam('username');
            $password = $this->getRequest()->getParam('password');
            //$frontend = $this->getRequest()->getParam('frontend');
            if(empty($key) || $key !== $_key )
                $response['error'] = Mage::helper("mobilecustomer")->__("Wrong KEY = ").$key.' ';
            if(empty($username) ) $response['error'] .= Mage::helper("mobilecustomer")->__("Need name. ");
            elseif(strlen($username)<6) $response['error'] .= Mage::helper("mobilecustomer")->__("Name length < 6. ");
            if (!Zend_Validate::is($username, 'EmailAddress'))
                $response['error'] .= Mage::helper("mobilecustomer")->__("Name is not email. ");

            if(empty($password) ) $response['error'] .= Mage::helper("mobilecustomer")->__("Need password. ");
            if(strlen($password)<6) $response['error'] .= Mage::helper("mobilecustomer")->__("Password length < 6. ");

            if(empty($response['error'])){
                /** @var Mage_Customer_Model_Session $session */
                $session = Mage::getSingleton('customer/session');
                try {
                    $session->login($username, $password);
                    if ($session->getCustomer()->getIsJustConfirmed()) {
                        $this->_welcomeCustomer($session->getCustomer(), true);
                    }
                    $response['success'] = Mage::helper('mobilecustomer')->__('Login successfully');
                    $response['CookieLifetime']=$session->getCookieLifetime();
                    $response['frontend']=$session->getSessionIdForHost();
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $value = Mage::helper('customer')->getEmailConfirmationUrl($username);
                            $message = Mage::helper('mobilecustomer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $response['error'] .= $message;
                    $session->setUsername($username);
                } catch (Exception $e) {
                    $response['error'] .= $e->getMessage();
                    Mage::log($response['error'], null, 'Auchan_MobileCustomer.log');
                }
            }
        }

        $jsonData = Mage::helper('core')->jsonEncode($response);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonData);
    }

    /**
     * Create customer account action
     * POST DOMEN/mobile-customer/account/create/?firstname=Andrey&middlename=&lastname=And&email=test@test.com&password=123456&confirmation=123456&key=GeJtDzII017yM
     * password confirmation  email lastname middlename firstname
     */
     public function CreateAction() {
         $response = array();
         if (!$this->getRequest()->isPost()) { // IF NOT POST
             $response['error'] = $this->__('Request is not POST.');
         }elseif($this->_getSession()->isLoggedIn()){ // IF LOGGED IN
             /** @var Mage_Customer_Model_Session $session */
             $session = $this->_getSession();
             $response['success'] = Mage::helper('mobilecustomer')->__('The user has been already logged in.');
             $response['error'] = Mage::helper('mobilecustomer')->__('The user has been already logged in.');
             $response['CookieLifetime']=$session->getCookieLifetime();
             $response['frontend']=$session->getSessionIdForHost();
         } else{
             /** @var Mage_Customer_Model_Session $session */
             $session = $this->_getSession();
             $session->setEscapeMessages(true); // prevent XSS injection in user input

             $customer = $this->_getCustomer();
//            //-------
//             $customer = $this->_getFromRegistry('current_customer');
//             if (!$customer) {
//                 $customer = Mage::getModel('customer/customer')->setId(null);
//             }
//             if ($this->getRequest()->getParam('is_subscribed', false)) {
//                 $customer->setIsSubscribed(1);
//             }
//             /**
//              * Initialize customer group id
//              */
//             $customer->getGroupId();
//             //------

            try {
                $errors = $this->_getCustomerErrors($customer);
                //$request = $this->getRequest();


                if (empty($errors)) {
                    $customer->cleanPasswordsValidationData();
                    $customer->save();
                    $this->_dispatchRegisterSuccess($customer);
                    $result_success = $this->_successProcessRegistration($customer);
                    $session = $this->_getSession();
                    $response['success'] .= $result_success ;
                    $response['CookieLifetime']=$session->getCookieLifetime();
                    $response['frontend']=$session->getSessionIdForHost();
                    //$response['success'] = Mage::helper('mobilecustomer')->__('Create successfully');
                } else {
                    $response['error'] .=implode($errors);
                }
            } catch (Mage_Core_Exception $e) {
                $session->setCustomerFormData($this->getRequest()->getPost());
                if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                    $url = $this->_getUrl('customer/account/forgotpassword');
                    $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                    $session->setEscapeMessages(false);
                } else {
                    $message = $e->getMessage();
                }
                $response['error'] .=implode($message);
            } catch (Exception $e) {
                $response['error'] .= $this->__('Cannot save the customer.').$e->getMessage();
                Mage::log($response['error'], null, 'Auchan_MobileCustomer.log');
            }
         }
        //die('die CreateAction() Auchan_MobileCustomer_AccountController ');
         $jsonData = Mage::helper('core')->jsonEncode($response);
         $this->getResponse()->setHeader('Content-type', 'application/json');
         $this->getResponse()->setBody($jsonData);
    }

    /**
     * Forgot customer password action
     * POST DOMEN/mobile-customer/account/forgotpassword/?email=test@test.com&key=GeJtDzII017yM
     */
    public function ForgotpasswordAction() {
        if (!$this->getRequest()->isPost()) { // IF NOT POST
            $response['error'] = $this->__('Request is not POST.');
        } else{
            $_key = Mage::helper("mobilecustomer")->getKey();
            $key  = $this->getRequest()->getParam('key');
            $email = (string) $this->getRequest()->getParam('email');
            if(empty($key) || $key !== $_key )
                $response['error'] = Mage::helper("mobilecustomer")->__("Wrong KEY = ").$key.' ';
            if (empty($email) || !Zend_Validate::is($email, 'EmailAddress'))
                $response['error'] .= Mage::helper("mobilecustomer")->__("Not valid email =  %s ",$email);


            if ( empty($response['error']) ) {
                /** @var $customer Mage_Customer_Model_Customer */
                $customer = Mage::getModel('customer/customer')
                    ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->loadByEmail($email);

                if ($customer->getId()) {
                    try {
                        $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
                        $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                        $customer->sendPasswordResetConfirmationEmail();
                        $response['success'] = Mage::helper('mobilecustomer')
                            ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',$email);

                    } catch (Exception $exception) {
                        $response['error'] = $exception->getMessage();
                    }
                }
                else{
                    $response['error'] .= Mage::helper('mobilecustomer')->__('No customer with a email = %s ',$email);
                }
            }
        }
        $jsonData = Mage::helper('core')->jsonEncode($response);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonData);

    }

    /**
     * Success Registration
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_AccountController
     */
    protected function _successProcessRegistration(Mage_Customer_Model_Customer $customer)
    {
        $response = array();
        $session = $this->_getSession();
        if ($customer->isConfirmationRequired()) {
            /** @var $app Mage_Core_Model_App */
            $app = $this->_getApp();
            /** @var $store  Mage_Core_Model_Store*/
            $store = $app->getStore();
            $customer->sendNewAccountEmail(
                'confirmation',
                $session->getBeforeAuthUrl(),
                $store->getId()
            );
            $customerHelper = $this->_getHelper('customer');
            $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.',
                $customerHelper->getEmailConfirmationUrl($customer->getEmail())));
            $response['success'] = $this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.',
                $customerHelper->getEmailConfirmationUrl($customer->getEmail()));
            //$url = $this->_getUrl('*/*/index', array('_secure' => true));
        } else {
            $session->setCustomerAsLoggedIn($customer);
            $response['success'] = Mage::helper('mobilecustomer')->__('Create successfully');
            //$url = $this->_welcomeCustomer($customer);
        }
        //$this->_redirectSuccess($url);
        return $response['success'];
    }

}